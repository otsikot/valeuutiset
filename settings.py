import tensorflow as tf

from reader import process_headers as input_processor

TRAINING_DATA_FILE = '/home/joni/Projects/valeuutiset/data/gutenberg.txt'
VALIDATION_DATA_FILE = '/home/joni/Projects/valeuutiset/data/gutenberg_valid.txt'
# TRAINING_DATA_FILE = '/home/joni/Projects/valeuutiset/data/training.txt'
# VALIDATION_DATA_FILE = '/home/joni/Projects/valeuutiset/data/validation.txt'

LOG_PATH = '/home/joni/Projects/valeuutiset/output/log/'
MODEL_SAVE_PATH = '/home/joni/Projects/valeuutiset/output/model/'

class ModelConfig:
    hidden_size = 2200
    number_of_layers = 2
    # Current (combined) training data has vocabulary size of 302
    vocab_size = 310
    data_type = tf.float32
    # Given vocabulary size above, this is ridiculously over-spec'd, but
    # Some embedding lookup doesn't like smaller datatypes
    input_dtype = tf.int32

class TrainingConfig:
    # (1500 * 3) * (70 * 120) -> OOM (after few epochs)
    # (1800 * 2) * (100 * 70) -> OOM
    # (1900 * 2) * (90 * 70) -> OOM
    # (2200 * 2) * (60 * 70) -> OOM
    batch_size = 50
    # Average length of header is ~ 64 (max is 222)
    num_steps = 70

    # http://torch.ch/blog/2016/07/25/nce.html uses 0.8 ?
    dropout_keep_prob = 0.5
    init_scale = 0.05

    # Doesn't matter much in practice, model is just trained as long as it takes
    number_of_epochs = 39

    # http://proceedings.mlr.press/v28/pascanu13.pdf suggests 0.5 - 10 times average norm
    max_gradient = 10.0 # http://torch.ch/blog/2016/07/25/nce.html
    learning_rate = 1e-4
