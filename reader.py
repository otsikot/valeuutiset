from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import logging
import pickle
import os
import re

from collections import namedtuple

import tensorflow as tf
import numpy as np

logger = logging.getLogger(__name__)

Input = namedtuple('Input', ['data', 'labels', 'epoch_size'])

class Encoder(object):
    SEPARATOR = '\n'
    UNKNOWN = '<unk>'

    def __init__(self, data, enc_dtype):
        self._dtype_ = enc_dtype
        self._frwd_ = {}
        self._rev_ = {}
        self.__add_coding__(self.SEPARATOR)
        self.__add_coding__(self.UNKNOWN)
        self.update(data)

    def __add_coding__(self, left):
        idx = self._dtype_.as_numpy_dtype(len(self._frwd_) + 1)
        self._frwd_[left] = idx
        self._rev_[idx] = left

    def update(self, data):
        for left in data:
            # We don't check _rev_ (We'll assume they are always updated synchronously)
            if left not in self._frwd_:
                self.__add_coding__(left)

    def right(self, left):
        return self._frwd_.get(left, self._frwd_.get(self.UNKNOWN))

    def left(self, right):
        return self._rev_.get(right, self.UNKNOWN)

    @property
    def dtype(self):
        return self._dtype_

    @property
    def size(self):
        return len(self._frwd_)

    def save(self, path):
        with open(os.path.join(path, 'encoder'), 'wb') as picklefile:
            pickle.dump(self, picklefile)

    @staticmethod
    def load(path):
        with open(os.path.join(path, 'encoder'), 'rb') as picklefile:
            return pickle.load(picklefile)

def _read_(filename):
    with open(filename, 'r') as infile:
        result = infile.read()

    return result


def _encode_chunk_(chunk, encoder):
    enc_chunk = []
    for c in chunk:
        enc_chunk.append(encoder.right(c))
    enc_chunk.append(encoder.right(encoder.SEPARATOR))
    return np.ndarray(
            shape=[len(enc_chunk)],
            dtype=encoder.dtype.as_numpy_dtype,
            buffer=np.array(enc_chunk)
        )


def process_text(input, encoder):
    output = []
    # Split input into chapters (=chunks separated by multiple newlines)
    chapters = re.split('\n\n+', input)
    for chapter in chapters:
        # Input chapter as one line
        chapter = ' '.join(chapter.split('\n'))
        output.append(_encode_chunk_(chapter, encoder))
    return output


def process_headers(input, encoder):
    output = []
    for row in input.split('\n'):
        if not row:
            continue
        output.append(_encode_chunk_(row, encoder))
    return output


def read_data(training_file, validation_file, input_dtype, encoder=None, processor=process_headers):
    training_data = _read_(training_file)
    validation_data = _read_(validation_file)

    used_chars = list(set(training_data) | set(validation_data))
    # Make encoder mapping stable at least with same input data
    used_chars.sort(key=lambda c: ord(c))
    if encoder is None:
        encoder = Encoder(used_chars, input_dtype)
    else:
        if encoder.dtype != input_dtype:
            raise Exception(
                "Encoder types don't match: {} vs {}".format(
                    encoder.dtype,
                    input_dtype
                )
            )
        encoder.update(used_chars)

    logger.debug("Encoder size {}".format(encoder.size))
    training_encoded = processor(training_data, encoder)
    validation_encoded = processor(validation_data, encoder)
    logger.debug("Data encoded!")
    return training_encoded, validation_encoded, encoder


# Shuffle elements on the input, join result together and use result as a epoch data
# (Divide to batches/steps and return as pair of tensors: (inputs, targets)
def input_producer(input, batch_size, num_steps, epoch_size):
    shuffled = np.random.permutation(input)
    data = np.concatenate(shuffled)
    epoch = data[:batch_size*(epoch_size*num_steps + 1)]
    shaped = epoch.reshape([batch_size, -1])
    inputs = []
    targets = []
    for i in range(epoch_size):
        input_slice =  shaped[: ,i*num_steps    : (i+1)*num_steps    ]
        target_slice = shaped[: ,i*num_steps + 1: (i+1)*num_steps + 1]
        inputs.append(input_slice)
        targets.append(target_slice)

    result = (
        np.stack(inputs),
        np.stack(targets)
    )
    return result


"""
    Divide raw data to batch_size batches and 'yield' each simultaneously in  num_steps-sized slices.
    (If raw_data size isn't divisible by batch_size * num_steps, some data is ignored)
"""
def batch_producer(raw_data, batch_size, num_steps, input_dtype, name=None):
    with tf.name_scope(name, "BatchProducer", [raw_data, batch_size, num_steps]):
        data_size = sum(len(item) for item in raw_data)

        # Divide data evenly among batches (round down, so we might ignore some data)
        batch_len = (data_size // batch_size)
        # How many executions each epoch contains (subtract one, so we have always one elemen 'left' for the last label)
        epoch_size = (batch_len - 1) // num_steps

        input_queue = tf.FIFOQueue(
            capacity=100,
            dtypes=[tf.int32, tf.int32],
            shapes=[
                [batch_size, num_steps],
                [batch_size, num_steps]
            ]
        )
        input_queue_op = input_queue.enqueue_many(
            tf.py_func(
                lambda: input_producer(raw_data, batch_size, num_steps, epoch_size),
                [],
                [tf.int32, tf.int32],
                stateful=True
            )
        )
        input_queue_runner = tf.train.QueueRunner(
            input_queue,
            [input_queue_op]
        )
        tf.train.add_queue_runner(input_queue_runner)
        input_data = input_queue.dequeue()
        inputs = input_data[0]
        targets = input_data[1]
        return Input(inputs, targets, epoch_size)
