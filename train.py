from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import datetime
import time
import os
import pickle
import sys
import logging
from collections import namedtuple

import numpy as np
import tensorflow as tf
from tensorflow.contrib.tensorboard.plugins import projector

import settings, reader
from model import Model
from training import get_cost, train
from util import StateRunner, generate_header, load_model_config, get_model_init

logger = logging.getLogger(__name__)

ValidatedModel = namedtuple('ValidatedModel', ['model', 'cost', 'epoch_size'])


# epoch_size
def evaluate_epoch(supervisor, session, epoch_number, training_config, model, training_op=None, logging_callback=None):
    """Runs the model on the given data."""
    start_time = time.time()
    costs = 0.0
    iterations = 0

    runner = StateRunner(session, model.model)

    # What values we want back
    fetches = {
        'cost': model.cost
    }

    if not training_op is None:
        fetches['training_op'] = training_op

    for step in range(model.epoch_size):
        if supervisor.should_stop():
            break

        # Reset state periodically (So we don't develop 'too much' context.)
        # There is not actually much shared context between headers (Unless we examine them grouped by publication time)
        if step % 250 == 0:
            runner.reset_state()

        # Run
        cost = runner.run(fetches)['cost']

        costs += cost
        iterations += training_config.num_steps

        if step % (model.epoch_size // 10) == 10:
            logger.debug(
                (
                    "%s %.1f perplexity: %.3f speed: %.0f wps" %
                    (
                        datetime.datetime.now().strftime("%H:%M:%S"),
                        epoch_number - 1 + step * 1.0 / model.epoch_size,
                        np.exp(costs / iterations),
                        iterations * training_config.batch_size / (time.time() - start_time)
                    )
                )
            )
            if logging_callback is not None:
                logging_callback()

    return np.exp(costs / iterations)


def produce_example_headers(session, model, input, encoder):

    for _ in range(5):
        header = generate_header(session, model, input, encoder)
        logger.debug(header)

def write_encoder_metadata(filename, encoder):
    with open(filename, 'w') as datafile:
        for i in range(encoder.size):
            datafile.write(encoder.left(i) + "\n")


def get_validated_model(data, model_config, training_config, initializer, reuse=None):
    input = reader.batch_producer(
        data,
        training_config.batch_size,
        training_config.num_steps,
        input_dtype=model_config.input_dtype,
        name="Data"
    )
    with tf.variable_scope("Model", reuse=reuse, initializer=initializer):
        model = Model(input.data, model_config, training_config.dropout_keep_prob)
        cost = get_cost(model, input.labels, training_config)

    return ValidatedModel(model, cost, input.epoch_size)


def save_embedding(embedding, encoder):
    metadata_file = os.path.join(settings.LOG_PATH, 'embedding.tsv')
    projector_config = projector.ProjectorConfig()
    projector_embedding = projector_config.embeddings.add()
    projector_embedding.tensor_name = embedding.name
    projector_embedding.metadata_path = metadata_file
    write_encoder_metadata(metadata_file, encoder)
    return projector_config

def save_model_config(model_config):
    config_file = os.path.join(settings.MODEL_SAVE_PATH, "model_config")
    with open(config_file, "wb") as outfile:
        settings_dict = {i: j for i, j in model_config.__class__.__dict__.items() if i[:2] != '__'}
        pickle.dump(settings_dict, outfile)

def main(argv):
    if len(argv) > 2:
        model_path = argv[1]
        checkpoint = argv[2]
    else:
        model_path = None
        checkpoint = None

    # If model path and checkpoint is provided, use
    # model config and encoder from saved model, otherwise create new
    if model_path and checkpoint:
        encoder = reader.Encoder.load(model_path)
        model_config = load_model_config(model_path)
    else:
        encoder = None
        model_config = settings.ModelConfig()

    training_config = settings.TrainingConfig()

    train_data, valid_data, encoder = reader.read_data(
        settings.TRAINING_DATA_FILE,
        settings.VALIDATION_DATA_FILE,
        input_dtype=model_config.input_dtype,
        encoder=encoder,
        processor=settings.input_processor
    )
    logger.debug("Encoder size {}".format(encoder.size))
    encoder.save(settings.MODEL_SAVE_PATH)

    save_model_config(model_config)

    if encoder.size >= model_config.vocab_size:
        raise Exception("Vocabulary size too small {} vs {}".format(model_config.vocab_size, encoder.size))

    with tf.Graph().as_default():
        initializer = tf.random_uniform_initializer(
            -training_config.init_scale,
            training_config.init_scale
        )

        with tf.name_scope("Train"):
            training_model = get_validated_model(
                train_data,
                model_config,
                training_config,
                initializer,
                reuse=None
            )
            tf.summary.scalar("Training_Loss", training_model.cost)
            training_op = train(training_model, training_model.cost, training_config)

        with tf.name_scope("Valid"):
            validation_model = get_validated_model(
                valid_data,
                model_config,
                training_config,
                initializer,
                reuse=True
            )
            tf.summary.scalar("Validation_Loss", validation_model.cost)

        # Try to pin produce to CPU to maybe save on memory
        with tf.device("/cpu:0"):
            with tf.name_scope("Produce"):
                with tf.variable_scope("Model", reuse=True, initializer=initializer):
                    produce_input = tf.placeholder(model_config.input_dtype, shape=[1,1])
                    produce_model = Model(produce_input, model_config)

        sv = tf.train.Supervisor(logdir=settings.LOG_PATH, init_fn=get_model_init(model_path, checkpoint))

        projector_config = save_embedding(training_model.model.embedding, encoder)
        projector.visualize_embeddings(sv.summary_writer, projector_config)

        with sv.managed_session() as session:
            try:
                for epoch_number in range(1, training_config.number_of_epochs + 1):
                    train_perplexity = evaluate_epoch(
                        sv,
                        session,
                        epoch_number,
                        training_config,
                        training_model,
                        training_op,
                        lambda : produce_example_headers(session, produce_model, produce_input, encoder)
                    )
                    if sv.should_stop():
                        break
                    logger.debug("Epoch: %d Train Perplexity: %.3f" % (epoch_number, train_perplexity))
                    valid_perplexity = evaluate_epoch(sv, session, epoch_number, training_config, validation_model)
                    if sv.should_stop():
                        break
                    logger.debug("Epoch: %d Valid Perplexity: %.3f" % (epoch_number, valid_perplexity))
            except KeyboardInterrupt:
                logger.warning("Caught keyboard interrupt")
                pass
            if settings.MODEL_SAVE_PATH:
                logger.debug("Saving model to %s." % settings.MODEL_SAVE_PATH)
                sv.saver.save(session, settings.MODEL_SAVE_PATH, global_step=sv.global_step)


if __name__ == "__main__":
    if not os.path.exists(settings.LOG_PATH):
        os.makedirs(settings.LOG_PATH)
    if not os.path.exists(settings.MODEL_SAVE_PATH):
        os.makedirs(settings.MODEL_SAVE_PATH)

    logger.setLevel(logging.DEBUG)
    con = logging.StreamHandler()
    con.setLevel(logging.DEBUG)
    logger.addHandler(con)
    f = logging.FileHandler(filename=os.path.join(settings.LOG_PATH, 'output.log'))
    f.setLevel(logging.DEBUG)
    logger.addHandler(f)
    tf.app.run(argv=sys.argv)
