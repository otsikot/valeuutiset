import logging
import pickle
import os

import numpy as np
import tensorflow as tf

logger = logging.getLogger(__name__)

class StateRunner:

    def __init__(self, session, model):
        self.session = session
        self.model = model
        self.state = session.run(model.initial_state)

    def run(self, fetches, feed_dict=None):

        if fetches is None:
            fetches = {}
        fetches['final_state'] = self.model.final_state

        if feed_dict is None:
            feed_dict = {}

        for i, c in enumerate(self.model.initial_state):
            feed_dict[c] = self.state[i]

        result = self.session.run(fetches, feed_dict)
        self.state = result.pop('final_state')
        return result

    def reset_state(self):
        self.state = self.session.run(self.model.initial_state)

# Tweak result distribution from model, to make generation bit more 'stable'
# (make distribution more extreme: increase odds of high-probability values as chop off low-probability ones)
def _fiddle_distribution_(distribution, position, hotness=0.07, ramp_off=7, debug=False):
    tmp = [i for i in distribution]
    tmp.sort(reverse=True)

    # As a first character, consider only those with probability at least 0.01 (= 1%)
    # After that increase limit linearly during ramp_off steps until value of hotness is reached
    # (This allows more randomness at the beginning when we don't have much state)
    min_probability = min(0.01 + position * (hotness / ramp_off), hotness)
    limit = 0
    for i, value in enumerate(tmp):
        if value < min_probability:
            break
        limit = value

    if limit == 0:
        limit = tmp[0]

    result = []
    possibilities = 0
    for i in distribution:
        if i >= limit:
            result.append(i)
            possibilities += 1
        else:
            result.append(0)

    if debug:
        logger.debug("limit {:.3} -> {}".format(min_probability, possibilities))
    total = sum(result)
    for i in range(len(result)):
        result[i] = result[i] / total

    return result


def generate_header(session, model, input, encoder):

    runner = StateRunner(session, model)
    fetches = {'softmax': model.softmax}

    # Pick newline as a starting point ("Start new header")
    input_data = [[encoder.right('\n')]]

    output = []
    position = 0
    while True:
        softmax = runner.run(fetches, {input: input_data})['softmax']
        dist = _fiddle_distribution_(softmax[0][0], position)
        result = np.random.choice(len(dist), p=dist)

        input_data = [[result]]

        result_char = encoder.left(result)
        if result_char == '\n':
            break

        # At the moment longest recorded header length is 222 and length drops of somewhat rapidly
        # (50th longest header length is 175). We'll assume that if we haven't gotten to the end until 250,
        # we aren't gonna.
        if position == 250:
            output.append('...')
            break

        output.append(result_char)
        position += 1

    return "".join(output)


def get_header_likelihood(header, session, model, input, encoder):
    runner = StateRunner(session, model)
    fetches =  { 'softmax': model.softmax }

    result = 0
    for position, header_char in enumerate(header):
        if position == (len(header)-1):
            break

        input_data = [[encoder.right(header_char)]]

        softmax = runner.run(fetches, {input: input_data})['softmax']
        dist = softmax[0][0]

        result += np.log(dist[encoder.right(header[position+1])])

    return np.e ** (result / len(header))


def load_model_config(model_path):
    config_file = os.path.join(model_path, 'model_config')
    with open(config_file, 'rb') as infile:
        conf_dict = pickle.load(infile)
        # For backwards compatibility
        if 'input_dtype' not in conf_dict:
            conf_dict['input_dtype'] = tf.int32
        class Config:
            pass
        result = Config()
        result.__dict__ = conf_dict
        return result


def get_model_init(model_path, checkpoint):
    if model_path and checkpoint:
        pre_train_saver = tf.train.Saver(tf.trainable_variables())

        def load_pretrain(sess):
            pre_train_saver.restore(
                sess,
                os.path.join(model_path, checkpoint)
            )

        return load_pretrain
    return None