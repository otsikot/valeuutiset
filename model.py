from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf


class Model(object):

    def __get_cell__(self, config, dropout_keep_prob):

        def cell():
            return tf.contrib.rnn.GRUCell(
                config.hidden_size,
                activation=tf.nn.softsign # https://deeplearning4j.org/lstm.html
            )

        # If we are training, and applying dropout, wrap cell in dropout-wrapper
        if dropout_keep_prob < 1:
            def wrapped_cell():
                return tf.contrib.rnn.DropoutWrapper(
                    cell(),
                    output_keep_prob=dropout_keep_prob
                )
        else:
            wrapped_cell = cell

        return wrapped_cell


    def __init__(self, input, config, dropout_keep_prob=1):
        self._input = input
        self._config = config

        # Layers
        cell = tf.contrib.rnn.MultiRNNCell(
            [self.__get_cell__(config, dropout_keep_prob)() for _ in range(config.number_of_layers)],
            state_is_tuple=True
        )

        # FIXME input.shape.dims[0] -> batch_size
        self._initial_state = cell.zero_state(input.shape.dims[0], config.data_type)

        # For performance reasons it is considered better to force this to cpu (instead of gpu)
        # https://www.tensorflow.org/performance/performance_guide
        with tf.device("/cpu"):
            self._embedding = embedding = tf.get_variable(
                "embedding",
                [config.vocab_size, config.hidden_size],
                dtype=config.data_type
            )
            # input_data is shape [batch_size, num_steps]
            # embedding is shape [vocab_size, size]
            # -> inputs is shape [batch_size, num_steps, size]
            inputs = tf.nn.embedding_lookup(embedding, input)

        # outputs is shape [batch_size, num_steps, size]
        outputs, self._final_state = tf.nn.dynamic_rnn(
            cell,
            inputs,
            initial_state=self._initial_state,
            swap_memory=True
        )

        # Output is shape [batch_size, num_steps , vocab_size]
        self._logits = logits = tf.layers.dense(
            inputs=outputs,
            units=config.vocab_size,
            use_bias=True,
            trainable=True,
            name="dense",
        )

        # Softmax logits, used when inferring/producing [batch_size, num_steps, vocab_size]
        self._softmax = tf.nn.softmax(logits)

    @property
    def input(self):
        return self._input

    @property
    def initial_state(self):
        return self._initial_state

    @property
    def final_state(self):
        return self._final_state

    @property
    def embedding(self):
        return self._embedding

    @property
    def logits(self):
        return self._logits

    @property
    def softmax(self):
        return self._softmax
