import logging
import sys

import secret
from producer import generate_headers

logger = logging.getLogger(__name__)


def _publish_(header):
    import twitter

    api = twitter.Api(consumer_key=secret.CONSUMER_KEY,
                      consumer_secret=secret.CONSUMER_SECRET,
                      access_token_key=secret.ACCESS_TOKEN_KEY,
                      access_token_secret=secret.ACCESS_TOKEN_SECRET)
    api.PostUpdate(header)


if __name__ == "__main__":
    argv = sys.argv
    headers = generate_headers(argv[1], argv[2], 1)
    header = headers[0]
    if len(header) < 140:
        logger.info("Publishing: '{}'".format(header))
        _publish_(header)
    else:
        logger.warning("Not publishing {}".format(header))

