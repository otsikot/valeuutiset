import tensorflow as tf
import settings


def get_cost(model, targets, training_config):
    loss = tf.nn.sparse_softmax_cross_entropy_with_logits(
        labels=targets,
        logits=model.logits
    )
    cost = tf.reduce_sum(loss) / training_config.batch_size

    return cost


def train(model, cost, training_config):
    optimizer = tf.train.AdamOptimizer(training_config.learning_rate)
    tvars = tf.trainable_variables()
    gradients = tf.gradients(
        cost,
        tvars,
        # Experimental aggregation might reduce memory usage
        aggregation_method=tf.AggregationMethod.EXPERIMENTAL_TREE
    )
    # According to https://arxiv.org/pdf/1308.0850.pdf (pg 5) clipping the norms is a good idea
    clipped_grads, _ = tf.clip_by_global_norm(gradients, training_config.max_gradient)

    train_op = optimizer.apply_gradients(
        zip(clipped_grads, tvars),
        global_step=tf.train.get_or_create_global_step()
    )

    return train_op