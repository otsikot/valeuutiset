import time
import logging
import os
import sys

import tensorflow as tf

from model import Model
from util import generate_header, load_model_config
import reader

logger = logging.getLogger(__name__)


def generate_headers(model_path, checkpoint, number):
    result = []
    init_start_time = time.time()
    encoder = reader.Encoder.load(model_path)

    config = load_model_config(model_path)

    with tf.name_scope("Produce"):
        with tf.variable_scope("Model", reuse=None):
            input = tf.placeholder(config.input_dtype, [1, 1])
            model = Model(input, config)

    pre_train_saver = tf.train.Saver(tf.trainable_variables())
    with tf.Session() as session:
        pre_train_saver.restore(session, os.path.join(model_path, checkpoint))
        init_end_time = time.time()
        logger.debug("Init done in {:.2} s. Starting generation...".format(init_end_time - init_start_time))
        for _ in range(number):
            start_time = time.time()
            header = generate_header(session, model, input, encoder)
            end_time = time.time()

            result.append(header)
            logger.debug("----{:.2}s----".format(end_time - start_time))
            logger.debug(header)

    return result

def main(argv):
    generate_headers(argv[1], argv[2], 20)

if __name__ == "__main__":
    logger.setLevel(logging.DEBUG)
    con = logging.StreamHandler()
    con.setLevel(logging.DEBUG)
    logger.addHandler(con)
    tf.app.run(argv=sys.argv)